# IPRTC: InterPlanetary webRTC

## Concept

* runs an IPFS node in browser (via [js-ipfs](https://github.com/ipfs/js-ipfs))
* DHT based (actually via proxy because the dht can't run in browser yet, but the IPFS people "are working on it", and it's "good enough" for now) node discovery and ICE signalling happens via IPFS pubsub
* WebRTC for the secure end-to-end channel
    - rudimentary chat is implemented
        + lists every node it sees, first click connects, 2nd sends a message
        + logs all messages
    - planned:
        + audio, video over WebRTC
        + automatic handling of IPFS links (because we can)
        + automatic storage of configs, contact list, etc in local IPFS node
            * option to export those (by sharing IPFS/IPNS links) for multiple devices
        + *essentially like telegram/threema/whatsapp/etc but without GMan stealing your data*

## Usage

for now there's just the "local server" version, since it's still in early proof of concept development. later on i'll publish it on ipfs to make it truly serverless.

### Install locally

```
npm install
```

### Run locally

```
npm run less && npm run coffee && npm start
```

### Open in local browser

Using a modern browser that supports WebRTC, like a recent version of Chrome or Firefox, open several windows of [http://localhost:12345](http://localhost:12345).

some config options are supported with "GET parameter like hashes" (`#key1=val1&key2=val2&...`):

* if you want to use a different ipfs repo (e.g. because you're testing in 2 tabs of the same browser), append  `#repo=<some name>` (e.g. [http://localhost:12345/#repo=2ndInstance](http://localhost:12345/#repo=2ndInstance))
* you can switch the ipfs proxy method from websocket to a faster but unstable webrtc gateway with `#mode=wrtc`
* to use the IPFS Companion app (`window.ipfs`), use `#mode=companion`
* also, in theory, connecting to an external ipfs node is supported, via `#mode=client&repo=/ip4/127.0.0.1/tcp/5001` (that address is also the default for client mode), but in practice this is currently unsupported by the ipfs library due to [this issue](https://github.com/ipfs/js-ipfs-http-client/issues/518) **this also applies to the companion connecting to an external node**

For all the gritty details, open the browser console and look at the logs.

## License

MIT
