IPFS = require 'ipfs'
IPFSClient = require 'ipfs-http-client'
Room = require 'ipfs-pubsub-room'
SPeer = require 'simple-peer'

window.jdenticon_config = {
  lightness: {
    color: [0.40, 0.80]
    grayscale: [0.38, 0.90]
  }
  saturation: {
    color: 0.50,
    grayscale: 0.00
  }
  backColor: "#00000000"
  #replaceMode: "observe"
}
jdenticon = require 'jdenticon'

cfg = window.location.hash.substr(1).split('&').reduce ((result,item)->
  sides = item.split '='
  result[sides[0]] = sides[1]
  return result
), {}

console.log 'Hash Config:', cfg

ipfs=null
if 'mode' of cfg and cfg.mode == 'companion'
  if window.ipfs and window.ipfs.enable
    console.log 'connecting via window.ipfs'
    ipfs = window.ipfs.enable({ commands: ['id', 'pubsub'] })
  else
    console.log 'no window.ipfs object!'
else if 'mode' of cfg and cfg.mode == 'client'
  repo = if 'repo' of cfg then cfg.repo else '/ip4/127.0.0.1/tcp/5001'
  console.log 'Connecting to IPFS api at ' + repo
  ipfs = IPFSClient(repo)
else
  repo = 'ipfs/iprtc-test/' + if 'repo' of cfg then cfg.repo else 'default'
  console.log 'Initializing IPFS node for repo ' + repo
  ipfs = new IPFS(
    repo: repo
    EXPERIMENTAL:
      pubsub: true
    config:
      Addresses:
        Swarm: [
          if 'mode' of cfg and cfg.mode == 'wrtc'
            '/dns4/wrtc-star.discovery.libp2p.io/tcp/443/wss/p2p-webrtc-star' # webrtc over ipfs over webrtc, almost as good as double rot13
          else
            '/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'
        ]
  )

class Peer
  constructor: (@discovery,@hash,@myhash) ->
    @peerListDOM = document.createElement'li'
    @peerListDOM.insertAdjacentHTML 'afterbegin', jdenticon.toSvg @hash, 16, 0
    @peerListDOM.appendChild document.createTextNode @hash
    @peerListDOM.onclick = =>
      if @state == 'disconnected'
        @connect()
      else
        msg = prompt 'Message to: '+ @hash
        if msg?
          @ssend msg
    document.getElementById'peers'.appendChild @peerListDOM
    console.log 'Peer '+@hash+' constructed'
    @setstate 'disconnected'
  connect: ->
    if @state != 'disconnected'
      return
    @timeout = setTimeout (=>
      @setstate 'disconnected'
      if @speer
        @speer.destroy()
    ), 60000
    @setstate 'find-initiator'
    @ownRan=Math.random()
    @peerRan=null
    console.log 'Peer '+@hash+' connecting, finding initiator...'
    @isend {state: @state, ran:@ownRan}
  setstate: (s) ->
    @state = s
    @peerListDOM.className = s
  isend: (msg)->
    console.log 'sending to '+@hash, msg
    @discovery.sendTo @hash, '/JSON/Peer/'+JSON.stringify msg
  irecv: (msgs)->
    if not msgs.startsWith '/JSON/Peer/'
      return
    msg = JSON.parse msgs.substr(11)
    console.log 'received from '+@hash, msg
    if msg.state == 'find-initiator'
      @peerRan = msg.ran
      if @state == 'disconnected'
        @connect()
      if @state == 'find-initiator'
        @initiator = ( @ownRan > @peerRan )
        console.log 'Peer '+@hash+' found initiator ('+@initiator+'), handshaking...'
        @setstate 'handshake'
        @speer = new SPeer { initiator: @initiator }
        @speer.on 'signal', (data)=>
          @isend {state: @state, signal: data}
        @speer.on 'connect', =>
          @setstate 'connected'
          clearTimeout(@timeout)
          @peerListDOM.className = @state
          console.log 'Peer '+@hash+' connected via webrtc!'
          #@ssend 'hi'
        @speer.on 'data', (data)=> @srecv data
    if msg.state == 'handshake'
      @speer.signal msg.signal
  ssend: (data)->
    console.log 'sending via webrtc to '+@hash+':',data
    @log data, 'send'
    @speer.send data
  srecv: (data)->
    console.log 'received via webrtc from '+@hash+':',data
    @log data, 'recv'
  log: (data,dir)->
    from = if dir == 'send' then @myhash else @hash
    to = if dir == 'recv' then @myhash else @hash
    tr = document.createElement'tr'
    tr.className = dir
    left = tr.appendChild document.createElement'td'
    left.insertAdjacentHTML 'afterbegin', jdenticon.toSvg @hash, 16, 0
    left.appendChild(document.createElement 'div').className = 'arrow ' + if dir == 'send' then 'left' else 'right'
    #left.insertAdjacentHTML 'afterbegin', jdenticon.toSvg from, 16, 0
    #left.appendChild(document.createTextNode from)
    #left.appendChild(document.createElement 'br')
    #left.insertAdjacentHTML 'beforeend', jdenticon.toSvg to, 16, 0
    #left.appendChild(document.createTextNode to)
    left.className = 'peer'
    right = tr.appendChild document.createElement'td'
    right.className = 'message'
    right.appendChild(document.createTextNode data)
    par = document.getElementById'log'
    par.insertBefore(tr,par.firstChild)
  destroy: ->
    document.getElementById'peers'.removeChild @peerListDOM
    if @speer
      @speer.destroy()


ipfsMain = =>
  ipfs.id (err, info)=>
    if err
      throw err

    console.log 'IPFS node ready with address ' + info.id

    document.title = 'IPRTC: ' + info.id

    document.getElementById'status'.insertAdjacentHTML 'afterbegin', jdenticon.toSvg info.id, 32, 0
    document.getElementById'status'.appendChild document.createTextNode info.id

    room = Room ipfs, '.de.nonchip.iprtc/discovery'

    # join / leave room

    peers = {}

    room.on 'peer joined', (peer) =>
      console.log 'peer ' + peer + ' joined'
      if not peers[peer]?
        console.log 'unseen peer, constructing'
        peers[peer] = new Peer room, peer, info.id
    room.on 'peer left', (peer) =>
      console.log 'peer ' + peer + ' left'
      if peers[peer]?
        console.log 'seen peer, destroying'
        peers[peer].destroy()
        peers[peer] = null

    # send and receive messages

    room.on 'message', (message) =>
      #console.log 'got message from ' + message.from + ': ' + message.data.toString()
      if peers[message.from]?
        peers[message.from].irecv(message.data.toString())
      else if message.from != info.id
        console.log 'unseen peer ' + message.from + ' discovered from message, constructing'
        peers[message.from] = new Peer room,message.from,info.id
        peers[message.from].irecv(message.data.toString())

    setInterval((=>
      room.broadcast 'heartbeat'
    ), 30000)

if 'mode' of cfg and cfg.mode == 'client'
  ipfsMain()
else if 'mode' of cfg and cfg.mode == 'companion'
  ipfs.then (ret)->
    ipfs = ret
    ipfsMain()
else
  ipfs.once 'ready', ipfsMain